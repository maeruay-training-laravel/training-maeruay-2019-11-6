<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::all();
        return view('book.index', compact('books'));
    }

    public function createPage()
    {
        return view('book.create');
    }

    public function create(Request $request)
    {
        $book = new Book();
        $book->name = $request->name;
        $book->author = $request->author;
        $book->price = $request->price;
        $book->describe = $request->describe;
        $book->type = $request->type;

        if (!$book->save()) {
            return redirect()->route('book.create.page');
        }
        return redirect()->route('book.page');
    }

    public function editPage(Book $book)
    {
        return view('book.edit', compact('book'));
    }

    public function edit(Request $request)
    {
        $book = Book::find($request->id);
        $book->name = $request->name;
        $book->author = $request->author;
        $book->price = $request->price;
        $book->describe = $request->describe;
        $book->type = $request->type;

        if (!$book->save()) {
            return redirect()->route('book.edit.page');
        }
        return redirect()->route('book.page');
    }

    public function delete(Request $request)
    {
        Book::find($request->id)->delete();
        return redirect()->route('book.page');
    }
}
