<?php

namespace Tests\Feature;

use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookTest extends TestCase
{
    use RefreshDatabase;

    public function testGuestCanSeeBookPage()
    {
        $response = $this->get(route('book.page'));
        $response->assertViewIs('book.index')
            ->assertStatus(200);
    }

    public function testGuestCanSeeAllBookExist()
    {
        $books = factory(Book::class, 5)->create();
        $response = $this->get(route('book.page'))
            ->assertSuccessful();

        $response->assertSee($books[0]->name);
        $response->assertSee($books[1]->name);
        $response->assertSee($books[2]->name);
        $response->assertSee($books[3]->name);
        $response->assertSee($books[4]->name);
    }

    public function testGuestCanAddNewBook()
    {
        $book = factory(Book::class)->make();

        $this->get(route('book.create.page'))
            ->assertViewIs('book.create')
            ->assertStatus(200);

        $this->post(route('book.create'), [
            '_token' => \Session::token(),
            'name' => $book->name,
            'author' => $book->author,
            'price' => $book->price,
            'describe' => $book->describe,
            'type' => $book->type,
        ])->assertRedirect(route('book.page'));

        $this->assertDatabaseHas('books', [
            'name' => $book->name,
            'author' => $book->author,
            'price' => $book->price,
            'describe' => $book->describe,
            'type' => $book->type,
        ]);

    }

    public function testGuestCanEditBook()
    {
        $book = factory(Book::class)->create();
        $bookEdit = factory(Book::class)->make();

        $this->get(route('book.edit.page', $book->id))
            ->assertViewIs('book.edit')
            ->assertSee($book->name)
            ->assertSee($book->author)
            ->assertSee($book->price)
            ->assertSee($book->describe)
            ->assertSee($book->type)
            ->assertStatus(200);

        $this->post(route('book.edit'), [
            '_token' => \Session::token(),
            'id' => $book->id,
            'name' => $bookEdit->name,
            'author' => $bookEdit->author,
            'price' => $bookEdit->price,
            'describe' => $bookEdit->describe,
            'type' => $bookEdit->type,
        ])->assertRedirect(route('book.page'));

        $bookEdited = Book::find($book->id);
        $this->assertEquals($bookEdit->name, $bookEdited->name);
        $this->assertEquals($bookEdit->author, $bookEdited->author);
        $this->assertEquals($bookEdit->price, $bookEdited->price);
        $this->assertEquals($bookEdit->describe, $bookEdited->describe);
        $this->assertEquals($bookEdit->type, $bookEdited->type);
    }

    public function testGuestCanDeleteBook()
    {
        $book = factory(Book::class)->create();

        $this->get(route('book.page'))
            ->assertSuccessful();

        $this->post(route('book.delete'), [
            '_token' => \Session::token(),
            'id' => $book->id,
        ])->assertRedirect(route('book.page'));

        $bookDeleted = Book::find($book->id);
        $this->assertEmpty($bookDeleted);
    }
}
