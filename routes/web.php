<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@index')->name('book.page');
Route::get('/create', 'BookController@createPage')->name('book.create.page');
Route::post('/create', 'BookController@create')->name('book.create');
Route::get('/edit/{book}', 'BookController@editPage')->name('book.edit.page');
Route::post('/edit', 'BookController@edit')->name('book.edit');
Route::post('/delete', 'BookController@delete')->name('book.delete');

Route::get('/test', function () {
    return view('test');
});
